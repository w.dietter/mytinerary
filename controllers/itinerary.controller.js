const mongoose = require('mongoose');
const ItineraryModel = require('../models/Itinerary');
const CommentModel = require('../models/Comment');

exports.getAllItineraries = async (req, res) => {
	const id = req.params.id;
	try {
		const itineraries = await ItineraryModel.find({
			city: mongoose.Types.ObjectId(id)
		})
			.populate({ path: 'comments', model: CommentModel })
			.populate('city', 'name');
		res.json(itineraries);
	} catch (error) {
		console.error(error);
	}
};

exports.getItinerary = async (req, res) => {
	const { itineraryId } = req.params;
	console.log(itineraryId);
	try {
		const itinerary = await ItineraryModel.findById(itineraryId).populate({
			path: 'comments',
			model: CommentModel
		});
		console.log(itinerary);

		if (!itinerary) {
			return res.status(400).json({
				message: 'Itinerary ot found.'
			});
		}

		return res.status(200).json(itinerary);
	} catch (error) {
		console.log(error);
	}
};

exports.getAllItineraries;
