const UserModel = require('../models/User');
const CommentModel = require('../models/Comment');
const ItineraryModel = require('../models/Itinerary');
const mongoose = require('mongoose');
const { validationResult } = require('express-validator');

exports.addComment = async (req, res) => {
	/* VALIDATION */
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({ errors: errors.array() });
	}

	const { itineraryId } = req.params;
	const { title, body } = req.body;
	const { id } = req.user;

	try {
		const user = await UserModel.findById(id);
		const itinerary = await ItineraryModel.findById(itineraryId);

		if (!user) {
			return res.status(400).json({
				message: 'User not found'
			});
		}

		if (!itinerary) {
			return res.status(400).json({
				message: 'Itinerary not found'
			});
		}

		const comment = new CommentModel({
			user: mongoose.Types.ObjectId(id),
			title,
			body
		});

		await comment.save();

		itinerary.comments.push(mongoose.Types.ObjectId(comment.id));

		await itinerary.save();
		console.log(itinerary);
		return res.status(201).json({
			message: 'Comment added.',
			comment,
			itinerary
		});
	} catch (error) {
		console.log(error);
	}
};

exports.deleteComment = async (req, res) => {
	const { itineraryId, commentId } = req.params;

	try {
		const itinerary = await ItineraryModel.findById(itineraryId);

		if (!itinerary) {
			return res.status(400).json({
				message: 'Itinerary not found'
			});
		}

		itinerary.comments = itinerary.comments.filter(comment => {
			return comment.toString() !== commentId;
		});

		await itinerary.save();

		return res.status(200).json({
			message: 'Comment deleted',
			comment: commentId,
			itinerary
		});
	} catch (error) {
		console.log(error);
	}
};

exports.getAllComments = async (req, res) => {
	const { itineraryId } = req.params;
	try {
		const itinerary = await ItineraryModel.findById(itineraryId).populate({
			path: 'comments',
			model: CommentModel
		});

		if (!itinerary) {
			return res.status(400).json({
				message: 'Itinerary not found.'
			});
		}

		return res.status(200).json({
			comments: itinerary.comments
		});
	} catch (error) {
		console.log(error);
	}
};
