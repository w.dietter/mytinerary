import React from 'react';
import { Link } from 'react-router-dom';
/* STYLED */
import {
	MtyWrapper,
	MtyFavourite,
	MtyDetailsContainer,
	MtyTitle,
	// MtyImage,
	MtyDetail,
	MtyHashtags,
	MtyLike
} from 'styled';
import { MdStar } from 'react-icons/md';
import { FaThumbsUp } from 'react-icons/fa';
/* REACTSTRAP */
import { Container, Row, Col } from 'reactstrap';
// import { BASE_URL } from 'constants/index.js';
/* COMPONENTS */
import ActivityList from 'components/Mytineraries/ActivityList';
/* REDUX */
import { connect } from 'react-redux';
import {
	addItineraryToUserFavs,
	likeItinerary,
	unlikeItinerary
} from 'redux/actions/user.actions';
import { getAllItineraries } from 'redux/actions/itinerary.actions';
import { userLikesSelector } from 'redux/reducers/user.reducer';

class Mytinerary extends React.Component {
	state = {};

	componentDidMount() {
		if (this.props.userLikes) {
			this.setState({
				canLike: !this.props.userLikes.includes(this.props._id)
			});
		}
	}

	handleLike = () => {
		if (this.state.canLike) {
			this.props.likeItinerary(this.props._id);
		} else {
			this.props.unlikeItinerary(this.props._id);
		}
		this.setState({
			canLike: !this.state.canLike
		});
	};

	render() {
		const {
			title,
			hashtag,
			duration,
			price,
			_id,
			user,
			likes,
			city
		} = this.props;
		const isFavourite = user && user.favItineraries.includes(_id) ? 1 : 0;
		return (
			<Container className='mt-3'>
				<MtyWrapper isfavourite={isFavourite}>
					{user && (
						<MtyFavourite
							isfavourite={isFavourite}
							onClick={() => {
								this.props.addItineraryToUserFavs(user._id, _id);
							}}>
							<MdStar />
						</MtyFavourite>
					)}
					<Row>
						<Col xs='4'>
							{/* <MtyImage
								src={`${BASE_URL}/images/${profilePic}`}
								alt=''
								className='img-fluid w-75'
							/> */}
						</Col>
						<Col xs='8'>
							<Row>
								<MtyTitle>
									<Link to={`/cities/${city._id}/itineraries/${_id}`}>
										{title}
									</Link>
								</MtyTitle>
							</Row>
						</Col>
					</Row>
					<Row>
						<MtyDetailsContainer>
							<MtyDetail center>
								<strong>Likes {likes}</strong>
								<MtyLike canLike={this.state.canLike}>
									<FaThumbsUp
										style={{ color: 'inherit' }}
										onClick={this.handleLike}
									/>
								</MtyLike>
							</MtyDetail>
							<MtyDetail center>
								{duration} {duration === 1 ? 'hour' : 'hours'}
							</MtyDetail>
							<MtyDetail center>$$ {price}</MtyDetail>
						</MtyDetailsContainer>
					</Row>
					<Row className='mt-3'>
						<MtyHashtags>
							{hashtag && hashtag.map(h => <span key={h}>#{h}</span>)}
						</MtyHashtags>
					</Row>
					<Row>
						<ActivityList itineraryId={_id} />
					</Row>
				</MtyWrapper>
			</Container>
		);
	}
}

const mapStateToProps = state => ({
	user: state.user.user,
	isAuthenticated: state.user.isAuthenticated,
	userLikes: userLikesSelector(state)
});

export default connect(mapStateToProps, {
	addItineraryToUserFavs,
	likeItinerary,
	unlikeItinerary,
	getAllItineraries
})(Mytinerary);
