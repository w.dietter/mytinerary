import React from 'react';
import styled from 'styled-components';
import { MtyTitle } from 'styled';
/* REDUX */
import { connect } from 'react-redux';
import { getItinerary } from 'redux/actions/itinerary.actions';

class Itinerary extends React.Component {
	componentDidMount() {
		this.props.getItinerary(this.props.match.params.itineraryId);
	}

	render() {
		const { selectedItinerary } = this.props;
		return (
			<>
				{this.props.selectedItinerary && (
					<>
						<MtyTitle>{selectedItinerary.title}</MtyTitle>
					</>
				)}
			</>
		);
	}
}

const mapStateToProps = state => ({
	selectedItinerary: state.itinerary.selectedItinerary
});

export default connect(mapStateToProps, { getItinerary })(Itinerary);
