const express = require('express');
const router = express.Router();
const { check } = require('express-validator');
const auth = require('../middleware/auth');

const {
	addComment,
	deleteComment,
	getAllComments
} = require('../controllers/comments.controller');

router
	.route('/itineraries/:itineraryId/comments')
	.get(auth, getAllComments)
	.post(
		auth,
		[
			check('title', 'title is required')
				.not()
				.isEmpty(),
			check('body', 'Empty messages are not allowed.')
				.not()
				.isEmpty()
		],
		addComment
	);

router
	.route('/itineraries/:itineraryId/comments/:commentId')
	.delete(auth, deleteComment);

module.exports = router;
