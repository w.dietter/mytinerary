const express = require('express');
const router = express.Router();

const {
	getAllItineraries,
	getItinerary
} = require('../controllers/itinerary.controller');

router
	.route('/cities/:id/mytineraries/all')

	/* GET /cities/:id/mytineraries/all */
	.get(getAllItineraries);

router.route('/mytineraries/:itineraryId').get(getItinerary);
module.exports = router;
